package com.test.orderservice.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.orderservice.dto.OrderDTO;

@RestController
public class OrderController {

	@GetMapping("/orders")
	public List<OrderDTO> getOrders() {
		
		List<OrderDTO>  orders = new ArrayList<>();
		orders.add(new OrderDTO(1L, "watch"));
		orders.add(new OrderDTO(2L, "bag"));

		return orders;

	}
	
}
